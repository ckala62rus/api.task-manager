<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/test', [\App\Http\Controllers\Auth\MeController::class, 'testAuth']);
    Route::post('auth/me', [\App\Http\Controllers\Auth\MeController::class, 'me']);

    //USER
    Route::apiResource('/user', \App\Http\Controllers\UserController::class);

    Route::apiResource('/project', \App\Http\Controllers\Admin\ProjectController::class);
    Route::apiResource('/tasks', \App\Http\Controllers\Admin\TaskController::class);
    Route::get('/all/tasks', [\App\Http\Controllers\Admin\TaskController::class, 'getTasksList']);
    Route::apiResource('/cost/category', \App\Http\Controllers\Admin\CostCategoryController::class);
    Route::apiResource('/cost/cost', \App\Http\Controllers\Admin\CostController::class);
});

Route::apiResource('permissions', \App\Http\Controllers\PermissionController::class);
Route::apiResource('roles', \App\Http\Controllers\RoleController::class);

Route::post(
    'permissions-roles/add-permission-role',
    [\App\Http\Controllers\PermissionRoleController::class, 'attachPermissionForRole']
);
Route::post(
    'permissions-roles/remove-permission-role',
    [\App\Http\Controllers\PermissionRoleController::class, 'detachPermissionForRole']
);

Route::post(
    'permissions-roles/attach-role-user',
    [\App\Http\Controllers\PermissionRoleController::class, 'attachRoleUser']
);
Route::post(
    'permissions-roles/detach-role-user',
    [\App\Http\Controllers\PermissionRoleController::class, 'detachRoleUser']
);

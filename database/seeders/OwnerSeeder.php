<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class OwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = Role::create([
            'name' => 'owner',
            'display_name' => 'Project Owner', // optional
            'description' => 'User is the owner of a given project', // optional
        ]);

        $user = User::create([
            'name' => 'Admin',
            'email' => 'qube100@yandex.ru',
            'password' => \Illuminate\Support\Facades\Hash::make('123123')
        ]);

        $user->attachRole($owner);
    }
}

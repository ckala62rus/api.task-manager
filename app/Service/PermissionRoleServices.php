<?php

namespace App\Service;

use App\Models\User;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;

class PermissionRoleServices
{
    /**
     * @var PermissionRepository
     */
    public PermissionRepository $permissionRepository;

    /**
     * @var RoleRepository
     */
    public RoleRepository $roleRepository;

    /**
     * @var UserRepository
     */
    public UserRepository $userRepository;

    /**
     * PermissionRoleServices constructor.
     * @param PermissionRepository $permissionRepository
     * @param RoleRepository $roleRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        PermissionRepository $permissionRepository,
        RoleRepository $roleRepository,
        UserRepository $userRepository
    ) {
        $this->permissionRepository = $permissionRepository;
        $this->roleRepository = $roleRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Attach permission for role
     * @param int $roleId
     * @param array $permissionIds
     * @return Model
     */
    public function attachPermissionRole(int $roleId, array $permissionIds): Model
    {
        $permissions = $this
            ->permissionRepository
            ->getRecordsByIds($permissionIds);

        $role = $this
            ->roleRepository
            ->getRecord($roleId);

        foreach ($permissions as $permission) {
            $role->attachPermission($permission);
        }

        $role = $this
            ->roleRepository
            ->getWith(
                $this
                    ->roleRepository
                    ->model(),
                $roleId,
                'permissions'
            );

        return $role;
    }

    /**
     * Detach permission for role
     * @param int $roleId
     * @param array $permissionIds
     * @return Model
     */
    public function detachPermissionRole(int $roleId, array $permissionIds): Model
    {
        $permissions = $this
            ->permissionRepository
            ->getRecordsByIds($permissionIds);

        $role = $this
            ->roleRepository
            ->getRecord($roleId);

        foreach ($permissions as $permission) {
            $role->detachPermission($permission);
        }

        $role = $this
            ->roleRepository
            ->getWith(
                $this
                    ->roleRepository
                    ->model(),
                $roleId,
                'permissions'
            );

        return $role;
    }

    /**
     * Attach role for user
     * @param array $data
     * @return Model
     */
    public function attachRoleUser(array $data): Model
    {
        $role = $this
            ->roleRepository
            ->getRecord($data['role_id']);

        $user = $this
            ->userRepository
            ->getRecord($data['user_id']);

        $user->attachRole($role);

        $user = $this
            ->userRepository
            ->getWith(
                $this
                    ->userRepository
                    ->model(),
                $user->id,
                'roles'
            );

        return $user;
    }

    /**
     * Detach role for user
     * @param array $data
     * @return Model
     */
    public function detachRoleUser(array $data): Model
    {
        $role = $this
            ->roleRepository
            ->getRecord($data['role_id']);

        $user = $this
            ->userRepository
            ->getRecord($data['user_id']);

        $user->detachRole($role);
        return $user;
    }

    /**
     * Delete all roles for user by id
     * @param int $id
     */
    public function deleteRolesForUser(int $id): void
    {
        $user = User::where('id', $id)
            ->with('roles')
            ->first();

        foreach ($user->roles as $role) {
            $user->detachRole($role);
        }
    }
}

<?php

namespace App\Service;

use App\Repositories\ProjectRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ProjectService
{
    /**
     * @var ProjectRepository
     */
    public ProjectRepository $projectRepository;

    /**
     * ProjectService constructor.
     * @param ProjectRepository $projectRepository
     */
    public function __construct(
        ProjectRepository $projectRepository
    ) {
        $this->projectRepository = $projectRepository;
    }

    /**
     * Get all projects
     * @return Collection
     */
    public function getAllProjects(): Collection
    {
        return $this
            ->projectRepository
            ->all();
    }

    /**
     * Get one project record
     * @param int $id
     * @return Model
     */
    public function getOneProject(int $id): Model
    {
        return $this
            ->projectRepository
            ->getRecord($id);
    }

    /**
     * Update one project by id
     * @param array $data
     * @param int $id
     * @return Model
     */
    public function updateProjectById(array $data, int $id): Model
    {
        return $this
            ->projectRepository
            ->update($data, $id);
    }

    /**
     * Create new project
     * @param array $data
     * @return Model
     */
    public function createProject(array $data): Model
    {
        return $this
            ->projectRepository
            ->store($data);
    }

    /**
     * Delete one project
     * @param int $id
     * @return bool
     */
    public function deleteProject(int $id): bool
    {
        return $this
            ->projectRepository
            ->destroy($id);
    }
}

<?php

namespace App\Service;

use App\Repositories\RoleRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class RoleServices
{
    /**
     * @var RoleRepository
     */
    public RoleRepository $roleRepository;

    /**
     * RoleServices constructor.
     * @param RoleRepository $roleRepository
     */
    public function __construct(
        RoleRepository $roleRepository
    ) {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Create one role
     * @param array $data
     * @return Model
     */
    public function createRole(array $data): Model
    {
        return $this
            ->roleRepository
            ->createRole($data);
    }

    /**
     * Get all roles
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getAllRoles(int $limit): LengthAwarePaginator
    {
        return $this
            ->roleRepository
            ->paginateAll($limit);
    }

    /**
     * Get one role by id
     * @param int $id
     * @return Model
     */
    public function getOneRole(int $id): Model
    {
        return $this
            ->roleRepository
            ->getRecord($id);
    }

    /**
     * Update one role by id
     * @param array $data
     * @param $id
     * @return Model
     */
    public function updateRole(array $data, $id): Model
    {
        return $this
            ->roleRepository
            ->update($data, $id);
    }

    /**
     * Delete one role by id
     * @param int $id
     * @return bool
     */
    public function destroyRole(int $id): bool
    {
        return $this
            ->roleRepository
            ->destroy($id);
    }
}

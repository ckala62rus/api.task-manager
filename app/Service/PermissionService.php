<?php

namespace App\Service;

use App\Repositories\PermissionRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class PermissionService
{
    /**
     * @var PermissionRepository
     */
    public PermissionRepository $permissionRepository;

    /**
     * PermissionService constructor.
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(
        PermissionRepository $permissionRepository
    ) {
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Create new permission
     * @param array $data
     * @return Model
     */
    public function createPermission(array $data): Model
    {
        return $this
            ->permissionRepository
            ->createPermission($data);
    }

    /**
     * Get all permissions
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getAllPermission(int $limit): LengthAwarePaginator
    {
        return $this
            ->permissionRepository
            ->paginateAll($limit);
    }

    /**
     * @param array $data
     * @param int $id
     * @return Model
     */
    public function updatePermission(array $data, int $id): Model
    {
        return $this
            ->permissionRepository
            ->update($data, $id);
    }

    /**
     * Get one permission by id
     * @param int $id
     * @return Model
     */
    public function getOnePermission(int $id): Model
    {
        return $this
            ->permissionRepository
            ->getRecord($id);
    }

    /**
     * Delete one permission by id
     * @param int $id
     * @return bool
     */
    public function destroyOnePermission(int $id): bool
    {
        return $this
            ->permissionRepository
            ->destroy($id);
    }
}

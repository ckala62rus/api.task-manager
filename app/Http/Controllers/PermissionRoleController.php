<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionRole\AttachOrDetachRoleUserRequest;
use App\Http\Requests\PermissionRole\PermissionRoleAttachRequest;
use App\Http\Resources\Role\RoleResource;
use App\Service\PermissionRoleServices;
use Symfony\Component\HttpFoundation\JsonResponse;

class PermissionRoleController extends Controller
{
    /**
     * @var PermissionRoleServices
     */
    public PermissionRoleServices $permissionRoleServices;

    /**
     * PermissionRoleController constructor.
     * @param PermissionRoleServices $permissionRoleServices
     */
    public function __construct(PermissionRoleServices $permissionRoleServices)
    {
        $this->permissionRoleServices = $permissionRoleServices;
    }

    /**
     * Attach permission for role
     * @param PermissionRoleAttachRequest $request
     * @return JsonResponse
     */
    public function attachPermissionForRole(PermissionRoleAttachRequest $request): JsonResponse
    {
        $data = $request->all();

        $role = $this
            ->permissionRoleServices
            ->attachPermissionRole($data['role_id'], $data['permission_ids']);

        return response()->json(
            new RoleResource($role),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Detach permission for role
     * @param PermissionRoleAttachRequest $request
     * @return JsonResponse
     */
    public function detachPermissionForRole(PermissionRoleAttachRequest $request): JsonResponse
    {
        $data = $request->all();

        $role = $this
            ->permissionRoleServices
            ->detachPermissionRole($data['role_id'], $data['permission_ids']);

        return response()->json(
            new RoleResource($role),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Attach role for user
     * @param AttachOrDetachRoleUserRequest $request
     * @return JsonResponse
     */
    public function attachRoleUser(AttachOrDetachRoleUserRequest $request): JsonResponse
    {
        $data = $request->all();

        $user =  $this
            ->permissionRoleServices
            ->attachRoleUser($data);

        return response()->json($user);
    }

    /**
     * Detach role for user
     * @param AttachOrDetachRoleUserRequest $request
     * @return JsonResponse
     */
    public function detachRoleUser(AttachOrDetachRoleUserRequest $request): JsonResponse
    {
        $data = $request->all();

        $user =  $this
            ->permissionRoleServices
            ->detachRoleUser($data);

        return response()->json($user);
    }
}

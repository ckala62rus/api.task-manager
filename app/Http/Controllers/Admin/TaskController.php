<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Task\TaskResource;
use App\Service\TaskService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * @var TaskService
     */
    public TaskService $taskService;

    /**
     * TaskController constructor.
     * @param TaskService $taskService
     */
    public function __construct(
        TaskService $taskService
    ) {
        $this->taskService = $taskService;
    }

    /**
     * Get all tasks
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $data = $request->all();

        $tasks = $this
            ->taskService
            ->getAllTasks($data);

        return response()->json([
            'data' => TaskResource::collection($tasks),
            'count' => count($tasks),
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Create new task
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->all();

        $task = $this
            ->taskService
            ->createNewTask($data);

        return response()->json($task, JsonResponse::HTTP_CREATED);
    }

    /**
     * Get one task
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $task = $this
            ->taskService
            ->getOneTask($id);

        return response()->json(new TaskResource($task), JsonResponse::HTTP_OK);
    }

    /**
     * Update one task by id
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $data = $request->all();

        $task = $this
            ->taskService
            ->updateTask($data, $id);

        return response()->json($task, JsonResponse::HTTP_CREATED);
    }

    /**
     * Delete one task by id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $data = $this
            ->taskService
            ->deleteTask($id);

        return response()->json($data, JsonResponse::HTTP_CREATED);
    }

    /**
     * Get all tasks for pagination
     * @param Request $request
     * @return JsonResponse
     */
    public function getTasksList(Request $request): JsonResponse
    {
        $data = $request->all();

        $tasks = $this
            ->taskService
            ->getAllTasksWithPagination($data);

        return response()->json([
            'data' => TaskResource::collection($tasks),
            'count' => $tasks->total()
        ], JsonResponse::HTTP_OK);
    }
}

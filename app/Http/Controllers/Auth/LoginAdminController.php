<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

class LoginAdminController extends LoginController
{
    public function username()
    {
        return 'email';
    }

    public function login(Request $request)
    {
        return $this->authenticateUsersTraitLogin($request);
    }
}

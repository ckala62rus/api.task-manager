<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\RoleCreateRequest;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Requests\Role\RoleUpdate;
use App\Http\Resources\Role\RoleResource;
use App\Service\RoleServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * @var RoleServices
     */
    public RoleServices $roleServices;

    /**
     * RoleController constructor.
     * @param RoleServices $roleServices
     */
    public function __construct(
        RoleServices $roleServices
    ) {
        $this->roleServices = $roleServices;
    }

    /**
     * Get all roles
     * @param RoleRequest $request
     * @return JsonResponse
     */
    public function index(RoleRequest $request): JsonResponse
    {
        $limit = $request->get('limit');

        $data = $this->roleServices->getAllRoles($limit);
        return response()->json([
            'data' => RoleResource::collection($data),
            'count' => $data->total()
        ]);
    }

    /**
     * Create new role
     * @param RoleCreateRequest $request
     * @return JsonResponse
     */
    public function store(RoleCreateRequest $request): JsonResponse
    {
        $data = $request->validated();

        $permissionIds = $request->only('permissions')['permissions'];

        $role = $this
            ->roleServices
            ->createRole($data);

        $role->syncPermissions($permissionIds);

        return response()->json(
            new RoleResource($role),
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * Get one role by id
     * @param int $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $role =  $this
            ->roleServices
            ->getOneRole($id);

        return response()->json(
            new RoleResource($role),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Update one role by id
     * @param RoleUpdate $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(RoleUpdate $request, $id): JsonResponse
    {
        $data = $request->validated();

        $permissionIds = $request->only('permissions')['permissions'];

        $role = $this
            ->roleServices
            ->updateRole($data, $id);

        $role->syncPermissions($permissionIds);

        return response()->json(
            new RoleResource($role),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Destroy role by id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $role = $this
            ->roleServices
            ->destroyRole($id);

        return response()->json(
            $role,
            JsonResponse::HTTP_OK
        );
    }
}

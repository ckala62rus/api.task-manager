<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\User\UserResource;
use App\Service\PermissionRoleServices;
use App\Service\UserService;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    public UserService $userService;

    /**
     * @var PermissionRoleServices
     */
    public PermissionRoleServices $permissionRoleServices;

    /**
     * UserController constructor.
     * @param UserService $userService
     * @param PermissionRoleServices $permissionRoleServices
     */
    public function __construct(
        UserService $userService,
        PermissionRoleServices $permissionRoleServices
    ) {
        $this->userService = $userService;
        $this->permissionRoleServices = $permissionRoleServices;
    }

    /**
     * Get all users
     * @param UserRequest $request
     * @return JsonResponse
     */
    public function index(UserRequest $request): JsonResponse
    {
        $limit = $request->get('limit');

        $users = $this
            ->userService
            ->getAllUsers($limit);

        return response()->json([
            'data'  => UserResource::collection($users),
            'count' => $users->total()
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Create new user
     * @param UserCreateRequest $request
     * @return JsonResponse
     */
    public function store(UserCreateRequest $request): JsonResponse
    {
        $data = $request->all();

        $users = $this
            ->userService
            ->createNewUser($data);

        return response()->json(
            new UserResource($users),
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * Get one record by id
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $user = $this
            ->userService
            ->getOneRecord($id);

        return response()->json(new UserResource($user), JsonResponse::HTTP_OK);
    }

    /**
     * Update one user by id
     * @param UserUpdateRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UserUpdateRequest $request, $id): JsonResponse
    {
        $data = $request->all();

        $this
            ->permissionRoleServices
            ->deleteRolesForUser($id);

        $this
            ->permissionRoleServices
            ->attachRoleUser([
            'user_id' => $data['id'],
            'role_id' => $data['role_id'],
        ]);

        $user = $this
            ->userService
            ->updateUser($data, $id);

        return response()->json(
            new UserResource($user),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Delete one user by id
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $data = $this
            ->userService
            ->destroyOneUser($id);

        return response()->json($data, JsonResponse::HTTP_OK);
    }
}

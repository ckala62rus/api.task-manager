<?php

namespace App\Http\Resources\Role;

use App\Http\Resources\Permission\PermissionCreateResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'display_name' => $this->display_name,
            'description' => $this->description,
            'permissions' => $this->getPermissionIds($this->permissions)
        ];
    }

    /**
     * @param $permissions
     * @return array
     */
    public function getPermissionIds($permissions): array
    {
        $ids = [];

        foreach ($permissions as $permission) {
            $ids[] = $permission->id;
        }

        return $ids;
    }
}

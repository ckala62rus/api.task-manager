<?php

namespace App\Http\Requests\Cost;

use Illuminate\Foundation\Http\FormRequest;

class CostCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rub' => 'required|integer',
            'penny' => 'required|max:2',
            'category_id' => 'required|integer',
            'description' => 'string',
            'date' => 'required|string',
        ];
    }
}

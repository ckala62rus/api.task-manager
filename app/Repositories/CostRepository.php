<?php

namespace App\Repositories;

use App\Models\Cost;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class CostRepository extends Repository
{
    /**
     * CostRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Cost();
    }

    /**
     * Get all task for project
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function allCosts(int $limit): LengthAwarePaginator
    {
        return $this
            ->model
            ->newQuery()
            ->with('category')
            ->paginate($limit);
    }

    /**
     * Get new query
     * @return Builder
     */
    public function newQuery(): Builder
    {
        return $this
            ->model
            ->newQuery();
    }

    /**
     * Search for date create
     * @param Builder $query
     * @param $date
     * @return Builder
     */
    public function whereDate(Builder $query, $date): Builder
    {
        return $query
            ->whereBetween('date', [$date['date_from'], $date['date_to']]);
    }

    /**
     * Search by category id
     * @param Builder $query
     * @param $data
     * @return Builder
     */
    public function whereCategory(Builder $query, $data): Builder
    {
        return $query
            ->where('category_id', $data['category']);
    }

    /**
     * Get paginate
     * @param Builder $query
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function queryPaginate(Builder $query, int $limit): LengthAwarePaginator
    {
        return $query->paginate($limit);
    }

    /**
     * Order by ASC or DESC
     * @param Builder $query
     * @return Builder
     */
    public function sort(Builder $query): Builder
    {
        return $query->orderBy('id', 'desc');
    }
}

<?php

namespace App\Repositories;

use App\Models\Project;

class ProjectRepository extends Repository
{
    /**
     * ProjectRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Project();
    }
}

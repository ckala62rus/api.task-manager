<?php

namespace App\Repositories;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;

class PermissionRepository extends Repository
{
    /**
     * PermissionRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Permission();
    }

    /**
     * Create new permission
     * @param array $data
     * @return Model
     */
    public function createPermission(array $data): Model
    {
        return $this
            ->model
            ->create($data);
    }
}
